
import init, { App, Config, Color } from './pkg/web_boids.js';

const STORAGE_KEY_PREFIX = 'web-boids';
const STORAGE_KEY_CONFIG = [STORAGE_KEY_PREFIX, 'config'].join('.');

(async function () {
    await init();

    function new_config(properties) {
        const config = new Config();
        Object.assign(config, properties);
        config.set_background(properties.background);
        for (const boid_color of properties.boid_colors) {
            config.add_boid_color(boid_color);
        }
        return config;
    }

    const defaultConfig = 'Birds';
    const configs = {
        'Fish': new_config({
            boid_count: 300,
            boid_scale: 0.2,
            depth: 1.0,
            sight_radius: 0.1,
            cohesion_factor: 0.004,
            alignment_factor: 0.008,
            separation_factor: 0.007,
            separation_radius: 0.04,
            target_factor: 0.001,
            bounds_padding: 0.05,
            out_of_bounds_speed: 0.003,
            max_speed: 0.2,
            background: '#222',
            boid_colors: [
                new Color(0.85, 0.15, 0.15),
                new Color(0.55, 1.0, 0.25),
                new Color(0.25, 0.55, 1.0),
                new Color(1.0, 0.65, 0.0),
            ]
        }),
        'Birds': new_config({
            boid_count: 2000,
            boid_scale: 0.05,
            depth: 2.0,
            sight_radius: 0.2,
            cohesion_factor: 0.004,
            alignment_factor: 0.009,
            separation_factor: 0.01,
            separation_radius: 0.05,
            target_factor: 0.001,
            bounds_padding: 0.05,
            out_of_bounds_speed: 0.003,
            max_speed: 0.2,
            background: '#adf url("sky.jpg") center/cover',
            boid_colors: [
                new Color(0.0, 0.0, 0.0),
            ]
        }),
    };
    const lastConfig = localStorage.getItem(STORAGE_KEY_CONFIG);
    let initialConfig;
    if (lastConfig !== null && configs.hasOwnProperty(lastConfig)) {
        initialConfig = lastConfig;
    } else {
        initialConfig = defaultConfig;
    }

    const canvas = document.querySelector('#canvas');
    const configSelect = document.querySelector('#config');
    const resetButton = document.querySelector('#reset');
    const playPauseButton = document.querySelector('#play-pause');
    const fpsText = document.querySelector('#fps');

    for (let [name, config] of Object.entries(configs)) {
        const option = document.createElement('option');
        option.value = name;
        option.textContent = name;
        configSelect.appendChild(option);
    }
    configSelect.value = initialConfig;

    const app = new App(canvas, configs[initialConfig]);

    let playing = false;
    let animationFrameHandle = null;
    let lastTimestamp = 0;
    let fpsCounter = 0;

    function update(timestamp) {
        animationFrameHandle = requestAnimationFrame(update);

        fpsCounter++;

        const deltaTime = lastTimestamp ? (timestamp - lastTimestamp) / 1000 : 0;
        lastTimestamp = timestamp;

        app.update(deltaTime);
    }

    setInterval(function () {
        const fps = fpsCounter;
        fpsCounter = 0;

        fpsText.textContent = fps.toFixed(2);
    }, 1000);

    function play() {
        if (playing) {
            return;
        }

        lastTimestamp = 0;
        animationFrameHandle = requestAnimationFrame(update);
        playing = true;
    }

    function pause() {
        cancelAnimationFrame(animationFrameHandle);
        playing = false;
    }

    function resize() {
        const w = window.innerWidth;
        const h = window.innerHeight;
        canvas.width = w;
        canvas.height = h;
        app.resize(w, h);
    }
    window.addEventListener('resize', resize);

    resize();

    app.reset();

    play();

    let mouseDown = false;

    configSelect.addEventListener('change', function () {
        const configName = configSelect.value;
        app.set_config(configs[configName]);
        localStorage.setItem(STORAGE_KEY_CONFIG, configName);
    });

    resetButton.addEventListener('click', function () {
        app.reset();
    });

    playPauseButton.addEventListener('click', function () {
        if (playing) {
            pause();
            playPauseButton.textContent = 'Play';
        } else {
            play();
            playPauseButton.textContent = 'Pause';
        }
    });

    canvas.addEventListener('mousedown', function (evt) {
        mouseDown = true;
        app.set_target(evt.clientX, evt.clientY);
    });

    window.addEventListener('mouseup', function (evt) {
        mouseDown = false;
        app.clear_target();
    });

    window.addEventListener('mousemove', function (evt) {
        if (mouseDown) {
            app.set_target(evt.clientX, evt.clientY);
        }
    });

    window.addEventListener('mouseout', function (evt) {
        app.clear_target();
    });

    let wasPlaying = false;
    window.addEventListener('visibilitychange', function () {
        if (document.hidden) {
            wasPlaying = playing;
            pause();
        } else {
            if (wasPlaying) {
                play();
            }
        }
    });

    if (window.location.hash === '#debug') {
        const dumpOctreeBtn = document.createElement('button');
        dumpOctreeBtn.textContent = 'Dump octree';
        document.querySelector('.controls').appendChild(dumpOctreeBtn);

        dumpOctreeBtn.addEventListener('click', function () {
            app.dump_octree();
        });
    }
})().catch(console.error);
