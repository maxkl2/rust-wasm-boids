
use js_sys::Math;

pub fn rand() -> f64 {
    Math::random()
}

pub fn rand_int(max: i32) -> i32 {
    (rand() * max as f64) as i32
}

pub fn rand_range(min: f64, max: f64) -> f64 {
    min + (max - min) * rand()
}
