
use crate::vector::Vector3;
use crate::aabb::AABB;
use crate::intersect::Intersect;
use crate::envelope::Envelope;

pub struct Sphere {
    center: Vector3,
    radius: f64,
    radius_squared: f64,
}

impl Sphere {
    pub fn new(center: Vector3, radius: f64) -> Sphere {
        Sphere {
            center,
            radius,
            radius_squared: radius.powi(2),
        }
    }
}

impl Intersect for Sphere {
    fn intersects(&self, other: &Sphere) -> bool {
        Vector3::distance_squared(&self.center, &other.center) < (self.radius + other.radius).powi(2)
    }
}

impl Intersect<AABB> for Sphere {
    fn intersects(&self, aabb: &AABB) -> bool {
        Vector3::distance_squared(&self.center, &aabb.closest_point(self.center)) < self.radius_squared
    }
}

impl Envelope for Sphere {
    fn contains_point(&self, p: &Vector3) -> bool {
        Vector3::distance_squared(&self.center, p) < self.radius_squared
    }
}
