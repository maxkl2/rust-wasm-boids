
use crate::vector::Vector3;
use crate::intersect::Intersect;

#[derive(Debug)]
pub struct AABB {
    min: Vector3,
    max: Vector3,
}

impl AABB {
    pub fn from_point(p: Vector3) -> AABB {
        AABB {
            min: p,
            max: p,
        }
    }

    pub fn from_corners(p1: Vector3, p2: Vector3) -> AABB {
        AABB {
            min: Vector3::min(&p1, &p2),
            max: Vector3::max(&p1, &p2),
        }
    }

    pub fn min(&self) -> Vector3 {
        self.min
    }

    pub fn max(&self) -> Vector3 {
        self.max
    }

    pub fn contains_point(&self, p: Vector3) -> bool {
        p.x >= self.min.x && p.y >= self.min.y && p.z >= self.min.z
            && p.x < self.max.x && p.y < self.max.y && p.z < self.max.z
    }

    pub fn closest_point(&self, p: Vector3) -> Vector3 {
        let x = if p.x < self.min.x {
            self.min.x
        } else if p.x > self.max.x {
            self.max.x
        } else {
            p.x
        };

        let y = if p.y < self.min.y {
            self.min.y
        } else if p.y > self.max.y {
            self.max.y
        } else {
            p.y
        };

        let z = if p.z < self.min.z {
            self.min.z
        } else if p.z > self.max.z {
            self.max.z
        } else {
            p.z
        };

        Vector3::new(x, y, z)
    }

    pub fn center(&self) -> Vector3 {
        Vector3::new((self.min.x + self.max.x) / 2.0,
                     (self.min.y + self.max.y) / 2.0,
                     (self.min.z + self.max.z) / 2.0)
    }
}

impl Intersect for AABB {
    fn intersects(&self, other: &AABB) -> bool {
        other.min.x < self.max.x && other.max.x > self.min.x
            && other.min.y < self.max.y && other.max.y > self.min.y
            && other.min.z < self.max.z && other.max.z > self.min.z
    }
}
