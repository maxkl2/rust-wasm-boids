
pub trait Intersect<Other=Self> {
    fn intersects(&self, other: &Other) -> bool;
}

// TODO: wait for https://github.com/rust-lang/rust/issues/37653
// default impl<T, U> Intersect<U> for T
// where
//     U: Intersect<T>,
// {
//     fn intersects(&self, other: &U) -> bool {
//         other.intersects(self)
//     }
// }
