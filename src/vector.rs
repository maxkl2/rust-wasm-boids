
use crate::rand::rand_range;
use std::fmt;

#[derive(Copy, Clone, Debug)]
pub struct Vector2 {
    pub x: f64,
    pub y: f64,
}

#[allow(dead_code)]
impl Vector2 {
    pub fn new(x: f64, y: f64) -> Vector2 {
        Vector2 {
            x,
            y,
        }
    }

    pub fn zero() -> Vector2 {
        Vector2 {
            x: 0.0,
            y: 0.0,
        }
    }

    pub fn rand(mag_min: f64, mag_max: f64, angle_min: f64, angle_max: f64) -> Vector2 {
        let mag = rand_range(mag_min, mag_max);
        let angle = rand_range(angle_min, angle_max);

        Vector2 {
            x: angle.cos() * mag,
            y: angle.sin() * mag,
        }
    }

    pub fn add(&self, other: &Vector2) -> Vector2 {
        Vector2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }

    pub fn add_in_place(&mut self, other: &Vector2) {
        self.x += other.x;
        self.y += other.y;
    }

    pub fn subtract(&self, other: &Vector2) -> Vector2 {
        Vector2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }

    pub fn subtract_in_place(&mut self, other: &Vector2) {
        self.x -= other.x;
        self.y -= other.y;
    }

    pub fn scale(&self, amount: f64) -> Vector2 {
        Vector2 {
            x: self.x * amount,
            y: self.y * amount,
        }
    }

    pub fn scale_in_place(&mut self, amount: f64) {
        self.x *= amount;
        self.y *= amount;
    }

    pub fn magnitude_squared(&self) -> f64 {
        self.x.powi(2) + self.y.powi(2)
    }

    pub fn magnitude(&self) -> f64 {
        self.magnitude_squared().sqrt()
    }

    pub fn distance_squared(&self, other: &Vector2) -> f64 {
        (self.x - other.x).powi(2) + (self.y - other.y).powi(2)
    }

    pub fn distance(&self, other: &Vector2) -> f64 {
        self.distance_squared(other).sqrt()
    }

    pub fn normalize(&self) -> Vector2 {
        let mag = self.magnitude();
        if mag > std::f64::EPSILON {
            self.scale(1.0 / mag)
        } else {
            Vector2::zero()
        }
    }

    pub fn normalize_in_place(&mut self) {
        let mag = self.magnitude();
        if mag > std::f64::EPSILON {
            self.scale_in_place(1.0 / mag);
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Vector3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

#[allow(dead_code)]
impl Vector3 {
    pub fn new(x: f64, y: f64, z: f64) -> Vector3 {
        Vector3 {
            x,
            y,
            z,
        }
    }

    pub fn from_polar(mag: f64, azimuth: f64, elevation: f64) -> Vector3 {
        Vector3 {
            x: azimuth.cos() * elevation.sin() * mag,
            y: azimuth.sin() * elevation.sin() * mag,
            z: elevation.cos() * mag,
        }
    }

    pub fn zero() -> Vector3 {
        Vector3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn rand(mag_min: f64, mag_max: f64, azimuth_min: f64, azimuth_max: f64, elevation_min: f64, elevation_max: f64) -> Vector3 {
        let mag = rand_range(mag_min, mag_max);
        let azimuth = rand_range(azimuth_min, azimuth_max);
        let elevation = rand_range(elevation_min, elevation_max);

        Vector3::from_polar(mag, azimuth, elevation)
    }

    pub fn add(&self, other: &Vector3) -> Vector3 {
        Vector3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }

    pub fn add_in_place(&mut self, other: &Vector3) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }

    pub fn subtract(&self, other: &Vector3) -> Vector3 {
        Vector3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }

    pub fn subtract_in_place(&mut self, other: &Vector3) {
        self.x -= other.x;
        self.y -= other.y;
        self.z -= other.z;
    }

    pub fn scale(&self, amount: f64) -> Vector3 {
        Vector3 {
            x: self.x * amount,
            y: self.y * amount,
            z: self.z * amount,
        }
    }

    pub fn scale_in_place(&mut self, amount: f64) {
        self.x *= amount;
        self.y *= amount;
        self.z *= amount;
    }

    pub fn magnitude_squared(&self) -> f64 {
        self.x.powi(2) + self.y.powi(2) + self.z.powi(2)
    }

    pub fn magnitude(&self) -> f64 {
        self.magnitude_squared().sqrt()
    }

    pub fn distance_squared(&self, other: &Vector3) -> f64 {
        (self.x - other.x).powi(2) + (self.y - other.y).powi(2) + (self.z - other.z).powi(2)
    }

    pub fn distance(&self, other: &Vector3) -> f64 {
        self.distance_squared(other).sqrt()
    }

    pub fn normalize(&self) -> Vector3 {
        let mag = self.magnitude();
        if mag > std::f64::EPSILON {
            self.scale(1.0 / mag)
        } else {
            Vector3::zero()
        }
    }

    pub fn normalize_in_place(&mut self) {
        let mag = self.magnitude();
        if mag > std::f64::EPSILON {
            self.scale_in_place(1.0 / mag);
        }
    }

    pub fn min(&self, other: &Vector3) -> Vector3 {
        Vector3::new(self.x.min(other.x),
                     self.y.min(other.y),
                     self.z.min(other.z))
    }

    pub fn max(&self, other: &Vector3) -> Vector3 {
        Vector3::new(self.x.max(other.x),
                     self.y.max(other.y),
                     self.z.max(other.z))
    }
}

impl fmt::Display for Vector3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}
