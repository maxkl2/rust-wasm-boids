
use std::fmt;

use crate::vector::Vector3;
use crate::aabb::AABB;
use crate::envelope::Envelope;
use crate::console_log;

const MAX_OBJECTS_PER_LEAF: usize = 10;
const MAX_DEPTH: usize = 20;

pub trait OctreeObject {
    fn position(&self) -> Vector3;
}

#[derive(Debug)]
enum NodeType<O: OctreeObject> {
    Inner { children: [Box<Node<O>>; 8] },
    Leaf { objects: Vec<Box<O>> },
}

struct NodeIter<'a, O: OctreeObject> {
    stack: Vec<&'a Node<O>>,
    current_leaf: Option<&'a Node<O>>,
    current_leaf_index: usize,
}

impl<'a, O: OctreeObject> NodeIter<'a, O> {
    fn new(root: &'a Node<O>) -> NodeIter<'a, O> {
        NodeIter {
            stack: vec![root],
            current_leaf: None,
            current_leaf_index: 0,
        }
    }
}

impl<'a, O: OctreeObject> Iterator for NodeIter<'a, O> {
    type Item = &'a Box<O>;

    fn next(&mut self) -> Option<&'a Box<O>> {
        loop {
            if let Some(leaf) = self.current_leaf {
                if let NodeType::Leaf { objects } = &leaf.typ {
                    if self.current_leaf_index < objects.len() {
                        let old_index = self.current_leaf_index;
                        self.current_leaf_index += 1;
                        return Some(&objects[old_index]);
                    }

                    self.current_leaf = None;
                } else {
                    unreachable!();
                }
            }

            // '?' to return None when the stack is empty
            let node = self.stack.pop()?;
            match &node.typ {
                NodeType::Inner { children } => {
                    self.stack.extend(children.iter().filter(|child| !child.empty).map(|child| child.as_ref()))
                },
                NodeType::Leaf { .. } => {
                    self.current_leaf = Some(node);
                    self.current_leaf_index = 0;
                },
            }
        }
    }
}

pub struct QueryIter<'a, O: OctreeObject, R: Envelope> {
    region: R,
    stack: Vec<&'a Node<O>>,
    current_leaf: Option<&'a Node<O>>,
    current_leaf_index: usize,
}

impl<'a, O: OctreeObject, R: Envelope> QueryIter<'a, O, R> {
    fn new(root: &'a Node<O>, region: R) -> QueryIter<'a, O, R> {
        QueryIter {
            region,
            stack: vec![root],
            current_leaf: None,
            current_leaf_index: 0,
        }
    }
}

impl<'a, O: OctreeObject, R: Envelope> Iterator for QueryIter<'a, O, R> {
    type Item = &'a Box<O>;

    fn next(&mut self) -> Option<&'a Box<O>> {
        loop {
            if let Some(leaf) = self.current_leaf {
                if let NodeType::Leaf { objects } = &leaf.typ {
                    while self.current_leaf_index < objects.len() {
                        let object = &objects[self.current_leaf_index];
                        self.current_leaf_index += 1;
                        if self.region.contains_point(&object.position()) {
                            return Some(object);
                        }
                    }

                    self.current_leaf = None;
                } else {
                    unreachable!();
                }
            }

            // '?' to return None when the stack is empty
            let node = self.stack.pop()?;
            match &node.typ {
                NodeType::Inner { children } => {
                    for child in children.iter() {
                        if !child.empty && self.region.intersects(&child.bounding_box) {
                            self.stack.push(child);
                        }
                    }
                },
                NodeType::Leaf { .. } => {
                    self.current_leaf = Some(node);
                    self.current_leaf_index = 0;
                },
            }
        }
    }
}

#[derive(Debug)]
struct Node<O: OctreeObject> {
    bounding_box: AABB,
    depth: usize,
    empty: bool,
    typ: NodeType<O>,
}

impl<O: OctreeObject> Node<O> {
    fn new(bounding_box: AABB, depth: usize) -> Node<O> {
        Node {
            bounding_box,
            depth,
            empty: true,
            typ: NodeType::Leaf { objects: Vec::new() },
        }
    }

    fn max_depth(&self) -> usize {
        match &self.typ {
            NodeType::Inner { children } => {
                children.iter()
                    .map(|child| child.max_depth())
                    .max()
                    .unwrap()
            },
            NodeType::Leaf { .. } => self.depth,
        }
    }

    fn object_count(&self) -> usize {
        match &self.typ {
            NodeType::Inner { children } => {
                children.iter()
                    .map(|child| child.object_count())
                    .sum()
            },
            NodeType::Leaf { objects } => objects.len(),
        }
    }

    fn display(&self, indent: usize, max_depth: Option<usize>, print_objects: bool) -> NodeDisplay<O> {
        NodeDisplay {
            node: &self,
            indent,
            max_depth,
            print_objects,
        }
    }

    fn insert(&mut self, object: Box<O>) -> bool {
        match &mut self.typ {
            NodeType::Inner { children } => {
                let pos = object.position();
                for child in children.iter_mut() {
                    if child.bounding_box.contains_point(pos) {
                        let success = child.insert(object);
                        if success {
                            self.empty = false;
                        }
                        return success;
                    }
                }
                false
            },
            NodeType::Leaf { objects } => {
                objects.push(object);
                self.empty = false;
                if objects.len() > MAX_OBJECTS_PER_LEAF && self.depth < MAX_DEPTH {
                    self.subdivide();
                }
                true
            },
        }
    }

    fn subdivide(&mut self) {
        assert!(matches!(self.typ, NodeType::Leaf { .. }), "only leaf nodes can be subdivided");

        let child_depth = self.depth + 1;
        let center = self.bounding_box.center();
        let min = self.bounding_box.min();
        let max = self.bounding_box.max();
        let children = [
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(max.x, max.y, max.z)), child_depth)), // +++
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(min.x, max.y, max.z)), child_depth)), // -++
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(max.x, min.y, max.z)), child_depth)), // +-+
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(min.x, min.y, max.z)), child_depth)), // --+
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(max.x, max.y, min.z)), child_depth)), // ++-
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(min.x, max.y, min.z)), child_depth)), // -+-
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(max.x, min.y, min.z)), child_depth)), // +--
            Box::new(Node::new(AABB::from_corners(center, Vector3::new(min.x, min.y, min.z)), child_depth)), // ---
        ];

        let mut tmp_typ = NodeType::Inner { children };

        std::mem::swap(&mut self.typ, &mut tmp_typ);

        // Re-insert the objects that were previously contained in this node
        if let NodeType::Leaf { mut objects } = tmp_typ {
            for object in objects.drain(..) {
                self.insert(object);
            }
        } else {
            // The assertion at the beginning of the method ensures that tmp_typ is a NodeType::Leaf after the swap
            unreachable!();
        }
    }

    fn query<R: Envelope>(&self, region: R) -> QueryIter<O, R> {
        QueryIter::new(self, region)
    }

    fn iter(&self) -> NodeIter<O> {
        NodeIter::new(self)
    }

    fn update(&mut self) -> Vec<Box<O>> {
        match &mut self.typ {
            NodeType::Inner { children } => {
                let mut empty = true;
                let mut maybe_bubble: Vec<Box<O>> = Vec::new();
                for child in children.iter_mut() {
                    let child_bubble = child.update();
                    if !child.empty {
                        empty = false;
                    }
                    maybe_bubble.extend(child_bubble);
                }
                let mut i = 0;
                while i < maybe_bubble.len() {
                    if self.bounding_box.contains_point(maybe_bubble[i].position()) {
                        let object = maybe_bubble.swap_remove(i);
                        let success = self.insert(object);
                        if success {
                            empty = false;
                        }
                    } else {
                        i += 1;
                    }
                }
                self.empty = empty;
                maybe_bubble
            },
            NodeType::Leaf { objects } => {
                let mut bubble = Vec::new();
                let mut i = 0;
                while i < objects.len() {
                    if !self.bounding_box.contains_point(objects[i].position()) {
                        let object = objects.swap_remove(i);
                        bubble.push(object);
                    } else {
                        i += 1;
                    }
                }
                self.empty = objects.is_empty();
                bubble
            },
        }
    }

    fn clear(&mut self) {
        match &mut self.typ {
            NodeType::Inner { children } => {
                for child in children.iter_mut() {
                    child.clear();
                }
                self.empty = true;
            },
            NodeType::Leaf { objects } => {
                objects.clear();
                self.empty = true;
            },
        }
    }
}

pub struct NodeDisplay<'a, O: OctreeObject> {
    node: &'a Node<O>,
    indent: usize,
    max_depth: Option<usize>,
    print_objects: bool,
}

impl<O: OctreeObject> fmt::Display for NodeDisplay<'_, O> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", "  ".repeat(self.indent))?;

        let empty_str = if self.node.empty { "empty" } else { "not empty" };

        match &self.node.typ {
            NodeType::Inner { children } => {
                let mut stop = false;
                if let Some(max_depth) = self.max_depth {
                    if self.node.depth >= max_depth {
                        stop = true;
                    }
                }

                if stop {
                    write!(f, "Inner: {} to {}, {} objects, max depth {}, {}", self.node.bounding_box.min(), self.node.bounding_box.max(), self.node.object_count(), self.node.max_depth(), empty_str)?;
                } else {
                    write!(f, "Inner: {} to {}, {}", self.node.bounding_box.min(), self.node.bounding_box.max(), empty_str)?;

                    for child in children.iter() {
                        writeln!(f, "")?;
                        write!(f, "{}", child.display(self.indent + 1, self.max_depth, self.print_objects))?;
                    }
                }
            },
            NodeType::Leaf { objects } => {
                write!(f, "Leaf: {} to {}, {} objects, {}", self.node.bounding_box.min(), self.node.bounding_box.max(), self.node.object_count(), empty_str)?;
                if self.print_objects {
                    for object in objects {
                        writeln!(f, "")?;
                        write!(f, "{}Object at {}", "  ".repeat(self.indent + 1), object.position())?;
                    }
                }
            },
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct Octree<O: OctreeObject> {
    root: Node<O>,
}

impl<O: OctreeObject> Octree<O> {
    pub fn new(bounding_box: AABB) -> Octree<O> {
        Octree {
            root: Node::new(bounding_box, 0),
        }
    }

    pub fn display(&self, max_depth: Option<usize>, print_objects: bool) -> OctreeDisplay<O> {
        OctreeDisplay {
            octree: &self,
            max_depth,
            print_objects,
        }
    }

    pub fn insert(&mut self, object: Box<O>) -> bool {
        self.root.insert(object)
    }

    pub fn query<'a, R: Envelope>(&self, region: R) -> impl Iterator<Item=&Box<O>> {
        self.root.query(region)
    }

    pub fn iter(&self) -> impl Iterator<Item=&Box<O>> {
        self.root.iter()
    }

    pub fn update(&mut self) {
        let lost_objects = self.root.update();
        if lost_objects.len() > 0 {
            console_log!("{} objects left the top level bounding box", lost_objects.len());
        }
    }

    pub fn clear(&mut self) {
        self.root.clear();
    }
}

pub struct OctreeDisplay<'a, O: OctreeObject> {
    octree: &'a Octree<O>,
    max_depth: Option<usize>,
    print_objects: bool,
}

impl<O: OctreeObject> fmt::Display for OctreeDisplay<'_, O> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Octree: {} objects, max depth {}", self.octree.root.object_count(), self.octree.root.max_depth())?;
        write!(f, "{}", self.octree.root.display(1, self.max_depth, self.print_objects))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::sphere::Sphere;
    use std::cell::Cell;

    struct Obj {
        pos: Cell<Vector3>,
    }

    impl Obj {
        fn new(pos: Vector3) -> Obj {
            Obj {
                pos: Cell::new(pos),
            }
        }
    }

    impl OctreeObject for Obj {
        fn position(&self) -> Vector3 {
            self.pos.get()
        }
    }

    impl fmt::Display for Obj {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Obj({})", self.pos.get())
        }
    }

    #[test]
    fn test_octree() {
        let mut o = Octree::new(AABB::from_corners(Vector3::new(-1.0, -1.0, -1.0), Vector3::new(1.0, 1.0, 1.0)));
        o.insert(Box::new(Obj::new(Vector3::new(0.1, 0.1, 0.1))));
        o.insert(Box::new(Obj::new(Vector3::new(0.2, 0.2, 0.2))));
        o.insert(Box::new(Obj::new(Vector3::new(-0.2, -0.2, -0.2))));

        println!("{}", o.display(None, true));

        println!("All objects:");
        for object in o.iter() {
            println!("  {}", object);
        }

        let query_region = Sphere::new(Vector3::new(0.2, 0.2, 0.0), 0.5);
        let found_objects: Vec<_> = o.query(query_region).collect();
        println!("Found objects:");
        for object in found_objects {
            println!("  {}", object);
        }

        for object in o.iter() {
            let pos = object.pos.get();
            if pos.x == 0.1 {
                let mut new_pos = pos.clone();
                new_pos.x = 0.5;
                object.pos.set(new_pos);
            }
        }

        println!("{}", o.display(None, true));

        o.update();

        println!("{}", o.display(None, true));
    }
}
