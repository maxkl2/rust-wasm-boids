
mod rand;
mod vector;
mod intersect;
mod envelope;
mod aabb;
mod sphere;
mod octree;
mod console_log;

use std::{panic, cmp};
use std::cell::Cell;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{HtmlCanvasElement, CanvasRenderingContext2d};

use crate::rand::{rand, rand_range, rand_int};
use crate::vector::Vector3;
use crate::octree::{Octree, OctreeObject};
use crate::sphere::Sphere;
use crate::aabb::AABB;

const TWO_PI: f64 = std::f64::consts::PI * 2.0;

#[wasm_bindgen]
#[derive(Copy, Clone, Default)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

#[wasm_bindgen]
impl Color {
    #[wasm_bindgen(constructor)]
    pub fn new(r: f64, g: f64, b: f64) -> Color {
        Color {
            r,
            g,
            b,
        }
    }

    fn to_rgb_string(&self) -> String {
        format!("rgb({}, {}, {})",
                (self.r * 255.0).round(),
                (self.g * 255.0).round(),
                (self.b * 255.0).round())
    }
}

#[wasm_bindgen]
#[derive(Clone, Default)]
pub struct Config {
    pub boid_count: u32,
    pub boid_scale: f64,
    pub depth: f64,
    pub sight_radius: f64,
    pub cohesion_factor: f64,
    pub alignment_factor: f64,
    pub separation_factor: f64,
    pub separation_radius: f64,
    pub target_factor: f64,
    pub bounds_padding: f64,
    pub out_of_bounds_speed: f64,
    pub max_speed: f64,

    background: String,

    boid_colors: Vec<Color>,
}

#[wasm_bindgen]
impl Config {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Config {
        Default::default()
    }

    pub fn set_background(&mut self, background: String) {
        self.background = background;
    }

    pub fn add_boid_color(&mut self, color: Color) {
        self.boid_colors.push(color);
    }
}

struct Boid {
    pos: Cell<Vector3>,
    v: Cell<Vector3>,
    color: Option<Color>,
    highlighted: bool,

    js_fill_style: Option<JsValue>,
}

impl Boid {
    fn new(pos: Vector3, v: Vector3, color: Option<Color>, highlighted: bool) -> Boid {
        let js_fill_style = color.map(|color| JsValue::from_str(&color.to_rgb_string()));
        Boid {
            pos: Cell::new(pos),
            v: Cell::new(v),
            color,
            highlighted,
            js_fill_style,
        }
    }

    fn draw(&self, context: &CanvasRenderingContext2d, config: &Config) {
        let pos = self.pos.get();
        let v = self.v.get();

        context.save();
        context.translate(pos.x, pos.y).unwrap();
        context.rotate(f64::atan2(v.y, v.x)).unwrap();

        let elevation = v.normalize().z.acos();
        // Boids get thinner when they are moving directly towards or away from the camera
        let fr = 1.0 - (elevation / (std::f64::consts::PI * 0.5) - 1.0).abs();
        // Boids get smaller when they are farther away
        let fz = 1.0 / (1.0 + pos.z * 0.5);

        context.begin_path();
        context.move_to(0.02 * fr * fz, 0.0);
        context.line_to(0.0, 0.01 * fz);
        context.line_to(-0.01 * fr * fz, 0.0);
        context.line_to(0.0, -0.01 * fz);
        context.close_path();

        if self.highlighted {
            context.set_fill_style(&JsValue::from_str("#fff"));
        } else {
            if let Some(js_fill_style) = &self.js_fill_style {
                context.set_fill_style(js_fill_style);
            }
        }
        context.fill();

        if self.highlighted {
            context.begin_path();
            context.arc(0.0, 0.0, config.sight_radius, 0.0, TWO_PI).unwrap();
            context.set_stroke_style(&JsValue::from_str("#06f"));
            context.set_line_width(0.002);
            context.stroke();
        }

        context.restore();
    }
}

impl OctreeObject for Boid {
    fn position(&self) -> Vector3 {
        self.pos.get()
    }
}

#[wasm_bindgen]
pub struct App {
    canvas: HtmlCanvasElement,
    context: CanvasRenderingContext2d,
    boids: Octree<Boid>,
    width: f64,
    scaled_width: f64,
    height: f64,
    scaled_height: f64,
    scaled_depth: f64,
    scale: f64,
    target: Option<Vector3>,

    draw_debug: bool,

    config: Config,

    single_boid_color_js_value: Option<JsValue>,
}

fn box_eq<T>(left: &Box<T>, right: &Box<T>) -> bool {
    let left: *const T = left.as_ref();
    let right: *const T = right.as_ref();
    left == right
}

#[wasm_bindgen]
impl App {
    #[wasm_bindgen(constructor)]
    pub fn new(canvas: HtmlCanvasElement, config: &Config) -> Result<App, JsValue> {
        let context: CanvasRenderingContext2d = canvas.get_context("2d")?
            .expect("canvas context identifier not supported")
            .dyn_into()?;

        let mut app = App {
            canvas,
            context,
            // TODO: correct size and resize when window is resized
            boids: Octree::new(AABB::from_corners(Vector3::new(-10.0, -10.0, -10.0), Vector3::new(10.0, 10.0, 10.0))),
            width: 0.0,
            scaled_width: 0.0,
            height: 0.0,
            scaled_height: 0.0,
            scaled_depth: 0.0,
            scale: 1.0,
            target: None,

            draw_debug: false,

            config: config.clone(),

            single_boid_color_js_value: None,
        };

        app.apply_config();

        Ok(app)
    }

    fn apply_config(&mut self) {
        self.scaled_depth = self.config.depth;

        if self.config.boid_colors.len() == 1 {
            self.single_boid_color_js_value = Some(JsValue::from_str(&self.config.boid_colors[0].to_rgb_string()))
        } else {
            self.single_boid_color_js_value = None;
        }

        self.canvas.style()
            .set_property("background", &self.config.background)
            .unwrap();
    }

    pub fn set_config(&mut self, config: &Config) {
        self.config = config.clone();

        self.apply_config();

        self.reset();
    }

    pub fn reset(&mut self) {
        let general_azimuth = rand_range(0.0, TWO_PI);
        let general_elevation = rand_range(0.0, std::f64::consts::PI);
        let azimuth_min = general_azimuth - 0.3;
        let azimuth_max = general_azimuth + 0.3;
        let elevation_min = general_elevation - 0.3;
        let elevation_max = general_elevation + 0.3;

        self.boids.clear();
        let boid_count = self.config.boid_count;
        let colors = &self.config.boid_colors;
        for i in 0..boid_count {
            let color = if self.single_boid_color_js_value.is_some() {
                None
            } else {
                Some(colors[rand_int(colors.len() as i32) as usize])
            };
            self.boids.insert(Box::new(Boid::new(
                Vector3::new(rand() * self.scaled_width, rand() * self.scaled_height, rand() * self.scaled_depth),
                Vector3::rand(0.1, 0.2, azimuth_min, azimuth_max, elevation_min, elevation_max),
                color,
                self.draw_debug && i >= boid_count - 5,
            )));
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.width = width as f64;
        self.height = height as f64;
        self.scale = cmp::min(width, height) as f64;
        self.scaled_width = self.width / self.scale;
        self.scaled_height = self.height / self.scale;
    }

    pub fn set_target(&mut self, x: u32, y: u32) {
        self.target = Some(Vector3::new(x as f64 / self.scale, y as f64 / self.scale, self.scaled_depth * 0.5));
    }

    pub fn clear_target(&mut self) {
        self.target = None;
    }

    pub fn enable_debug(&mut self, enable: bool) {
        self.draw_debug = enable;
    }

    pub fn dump_octree(&self) {
        console_log!("{}", self.boids.display(None, true));
    }

    pub fn update(&mut self, delta_time: f64) {
        self.step(delta_time);

        self.draw();
    }

    fn step(&mut self, delta_time: f64) {
        let separation_radius_squared = self.config.separation_radius.powi(2);
        let max_speed_squared = self.config.max_speed.powi(2);

        for boid in self.boids.iter() {
            let boid_pos = boid.pos.get();
            let boid_v = boid.v.get();

            let mut group_center_sum = Vector3::zero();
            let mut group_v_sum = Vector3::zero();
            let mut separation = Vector3::zero();
            let mut visible_count = 0;

            let visible_boids = self.boids.query(Sphere::new(boid_pos, self.config.sight_radius));
            for visible_boid in visible_boids {
                if box_eq(visible_boid, boid) {
                    continue;
                }

                visible_count += 1;

                let visible_boid_pos = visible_boid.pos.get();
                let visible_boid_v = visible_boid.v.get();

                let dist_squared = boid_pos.distance_squared(&visible_boid_pos);

                group_center_sum.add_in_place(&visible_boid_pos);
                group_v_sum.add_in_place(&visible_boid_v);

                if dist_squared < separation_radius_squared {
                    let dist = dist_squared.sqrt();
                    let f = (self.config.separation_radius - dist) / self.config.separation_radius;
                    separation.add_in_place(&boid_pos.subtract(&visible_boid_pos).normalize().scale(f));
                }
            }

            let v_separation = separation.scale(self.config.separation_factor);
            let v_alignment = if visible_count > 0 {
                let group_v = group_v_sum.scale(1.0 / visible_count as f64);
                group_v.subtract(&boid_v).scale(self.config.alignment_factor)
            } else {
                Vector3::zero()
            };
            let v_cohesion = if visible_count > 0 {
                let group_center = group_center_sum.scale(1.0 / visible_count as f64);
                group_center.subtract(&boid_pos).scale(self.config.cohesion_factor)
            } else {
                Vector3::zero()
            };
            let v_bounds = {
                let x = if boid_pos.x < self.config.bounds_padding {
                    self.config.out_of_bounds_speed
                } else if boid_pos.x > self.scaled_width + self.config.bounds_padding {
                    -self.config.out_of_bounds_speed
                } else {
                    0.0
                };
                let y = if boid_pos.y < self.config.bounds_padding {
                    self.config.out_of_bounds_speed
                } else if boid_pos.y > self.scaled_height + self.config.bounds_padding {
                    -self.config.out_of_bounds_speed
                } else {
                    0.0
                };
                let z = if boid_pos.z < self.config.bounds_padding {
                    self.config.out_of_bounds_speed
                } else if boid_pos.z > self.scaled_depth + self.config.bounds_padding {
                    -self.config.out_of_bounds_speed
                } else {
                    0.0
                };
                Vector3::new(x, y, z)
            };
            let v_target = if let Some(target) = &self.target {
                target.subtract(&boid_pos).normalize().scale(self.config.target_factor)
            } else {
                Vector3::zero()
            };

            let mut new_pos = boid_pos;
            let mut new_v = boid_v;

            new_v.add_in_place(&v_separation);
            new_v.add_in_place(&v_alignment);
            new_v.add_in_place(&v_cohesion);
            new_v.add_in_place(&v_bounds);
            new_v.add_in_place(&v_target);

            if new_v.magnitude_squared() > max_speed_squared {
                new_v.normalize_in_place();
                new_v.scale_in_place(self.config.max_speed);
            }

            new_pos.add_in_place(&new_v.scale(delta_time));

            boid.pos.set(new_pos);
            boid.v.set(new_v);
        }

        self.boids.update();
    }

    fn draw(&self) {
        self.context.clear_rect(0.0, 0.0, self.width, self.height);

        self.context.save();
        self.context.scale(self.scale, self.scale).unwrap();

        if let Some(color) = &self.single_boid_color_js_value {
            self.context.set_fill_style(color);
        }

        for boid in self.boids.iter() {
            boid.draw(&self.context, &self.config);
        }

        if self.draw_debug {
            if let Some(target) = &self.target {
                self.context.begin_path();
                self.context.arc(target.x, target.y, 0.01, 0.0, TWO_PI).unwrap();
                self.context.set_fill_style(&JsValue::from_str("#ff0"));
                self.context.fill();
            }
        }

        self.context.restore();
    }
}

#[wasm_bindgen(start)]
pub fn start() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
}
