
use crate::vector::Vector3;
use crate::intersect::Intersect;
use crate::aabb::AABB;

pub trait Envelope: Intersect<AABB> {
    fn contains_point(&self, p: &Vector3) -> bool;
}
